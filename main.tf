terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.6.0"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

data "gitlab_group" "sandbox" {
  full_path = "pedrodavid-sandbox"
}

# resource "gitlab_project" "gitlab_manager" {
#   name = "Gitlab Manager"
#   namespace_id = data.gitlab_group.sandbox.id
# }

resource "gitlab_project" "test_project_1" {
  namespace_id = data.gitlab_group.sandbox.id
  name = "Test Project 1"

  description = "Gitlab Manager generated"
}
